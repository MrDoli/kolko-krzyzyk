package View;

/**
 * View z MVC dla WelcomeWindow
 */
public class PlanszaView {
    private Plansza plansza;

    /**
     * Konstrujemy Plansze w View pochodzącym z MVC
     * @param name imie/nick gracza
     * @param who zmienna informująca o tym czy to "server" czy "client" - tylko do wyświetlania (w utworzonej planszy)
     */
    public PlanszaView(String name, String who)
    {
        plansza=new Plansza(name, who);
    }

    public Plansza getPlanszaFromView(){
        return this.plansza.getPlansza();
    }
}
