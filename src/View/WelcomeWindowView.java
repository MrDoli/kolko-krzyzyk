package View;

/**
 * View z MVC dla WelcomeWindow
 */
public class WelcomeWindowView {
    private WelcomWindow welcomWindow;

    /**
     * Konstruktor View.WelcomeWindowView tworzy View Welcome Window
     */
    public WelcomeWindowView(){
        welcomWindow=new WelcomWindow();
    }

    public WelcomWindow getWelcomWindow() {
        return welcomWindow;
    }

    public void hideWindow(){
        welcomWindow.setVisible(false);
    }
}
