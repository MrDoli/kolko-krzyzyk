package View;

import Model.ElectedListener;
import Model.WelcomeWindowModel;

import javax.swing.*;
import java.awt.*;

/**
 * Tworzenie wyglądu okna WelcomeWindow
 */
public class WelcomWindow extends JFrame {
    public static void main(String[] args) {
        WelcomWindow welcomWindow = new WelcomWindow();
    }

    private JLabel serverAdress;
    private JLabel nick;
    private JTextField forServerAdress;
    private JTextField forNick;
    private JButton ok;
    private JButton x;
    private JButton o;
    private JLabel text;
    private ElectedListener electedListener;
    private ElectedListener electedListenerOk;

    /**
     * Konstruktor tworzący Welcome Widndow, odpowiada za utworzenie i rozłożenie odpowienich elemntów
     */
    public WelcomWindow() {
        final int width = 500;
        final int high = 320;
        serverAdress = new JLabel("Adres Servera: ");
        nick = new JLabel("Nick: ");
        forServerAdress = new JTextField("loclahost");
        forServerAdress.setPreferredSize(new Dimension(width / 2, 50));
        forNick = new JTextField();
        forNick.setPreferredSize(new Dimension(width / 2, 50));
        ok = new JButton("Ok");
        setTitle("Welcome window");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(width, high);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        p.setPreferredSize(new Dimension(width / 2, high));
        serverAdress.setFont(new Font("Arial", Font.PLAIN, 30));
        nick.setFont(new Font("Arial", Font.PLAIN, 30));
        //p.setBorder(BorderFactory.createEmptyBorder(1//,10,10,10));
        //p.add(Box.createHorizontalGlue());

        p.add(serverAdress);
        p.add(Box.createRigidArea(new Dimension(10, 50)));   // Odstęp pomiędzy Labelami
        p.add(nick);

        JPanel q = new JPanel();
        q.setLayout(new BoxLayout(q, BoxLayout.Y_AXIS));
        q.setPreferredSize(new Dimension(width / 2, high));
        forServerAdress.setFont(new Font("Arial", Font.PLAIN, 20));
        forNick.setFont(new Font("Arial", Font.PLAIN, 20));
        q.add(forServerAdress);
        q.add(Box.createRigidArea(new Dimension(10, 10)));   // Odstęp pomiędzy Labelami
        q.add(forNick);

        JPanel r = new JPanel();
        ok.setPreferredSize(new Dimension(60, 40));
        r.add(ok);

        x = new JButton("X");
        o = new JButton("O");
        x.setPreferredSize(new Dimension(width / 2, 50));
        o.setPreferredSize(new Dimension(width / 2, 50));
        x.setFont(new Font("Arial", Font.PLAIN, 30));
        o.setFont(new Font("Arial", Font.PLAIN, 30));
        text = new JLabel("Kim chcesz byc?");
        text.setHorizontalAlignment(JLabel.CENTER);
        text.setFont(new Font("Arial", Font.PLAIN, 20));
        JPanel u = new JPanel();
        u.setLayout(new BorderLayout());
        u.add(text, BorderLayout.NORTH);
        q.add(Box.createRigidArea(new Dimension(10, 10)));   // Odstęp pomiędzy Labelami
        u.add(x, BorderLayout.CENTER);
        q.add(Box.createRigidArea(new Dimension(10, 10)));   // Odstęp pomiędzy Labelami
        u.add(o, BorderLayout.EAST);

        this.setVisible(true);
        this.getContentPane().add(BorderLayout.CENTER, q);
        this.getContentPane().add(BorderLayout.WEST, p);
        this.getContentPane().add(BorderLayout.SOUTH, r);
        this.getContentPane().add(BorderLayout.NORTH, u);
    }

    /**
     * Dodaje słuchacza dla przycisków X/O
     * @param button przycisk X/O
     * @param welcomeWindowModel model dla Welcome Window
     */
    public void addElectedButtonListener(JButton button, WelcomeWindowModel welcomeWindowModel) {
        electedListener = new ElectedListener(button, welcomeWindowModel);
    }

    /**
     * Dodaje słuchacza do przycisku "OK"
     * @param buttonIn przycisk "OK"
     * @param textFieldIn obszar tekstowy z imieniem/nickiem gracza
     * @param welcomeWindowModelIn model dla Welcome Window
     */
    public void addOkButtonListener(JButton buttonIn,JTextField textFieldIn, WelcomeWindowModel welcomeWindowModelIn)
    {
        electedListenerOk=new ElectedListener(buttonIn,textFieldIn, welcomeWindowModelIn);
    }

    public JButton getO() {
        return o;
    }

    public JButton getx() {
        return x;
    }

    public JTextField getForNick() {
        return forNick;
    }

    public JButton getOk() {
        return ok;
    }

    public void hideWindow(){
        this.setVisible(false);
    }
}
