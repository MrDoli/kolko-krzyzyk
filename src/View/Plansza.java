package View;

import Model.PlanszaModel;
import Model.PushListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Tworzenie wyglądu okna Planszy
 */
public class Plansza extends JFrame{
    private JLabel textName;
    private JButton[] buttony = new JButton[25];
    private PushListener PushListener;
    private String theName;

    /**
     * Konstruktor tworzy okno
     * @param name imie/nick gracza
     * @param who zmienna informująca o tym czy to "server" czy "client" - tylko do wyświetlania
     */
    public Plansza(String name, String who)
    {
        theName=name;
        textName=new JLabel(name);

        final int width = 650;    //moze enum
        final int high = 700;
        setTitle("Kolko - Krzyzyk "+who);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(width, high);
        //this.setVisible(false);
        // POZWALA USTAWIC OKNO NA SRODKU EKRANU
        Dimension dim=Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2,dim.height/2-this.getSize().height/2);

        JPanel p = new JPanel(new GridLayout()); // ABY STOSOWAL DOMYSLNIE GridLayout()
        p.setPreferredSize(new Dimension(600, 600)); //szerokosc, wysokosc
        p.setLayout(new GridLayout(5, 5, 0, 0));

        for (int i = 0; i < buttony.length; i++) {
            int finalI=i;
            buttony[i] = new JButton();
            buttony[i].setPreferredSize(new Dimension(width / 5, width / 5));
            p.add(buttony[i]);
        }

        JPanel q = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        q.setPreferredSize(new Dimension(600, 60));

        textName.setPreferredSize(new Dimension(600, 60));
        textName.setVerticalTextPosition(FlowLayout.CENTER);
        firstPaint(who);
        q.add(textName);

        firstPosition(buttony);

        this.setVisible(true);
        this.getContentPane().add(BorderLayout.CENTER, p);
        this.getContentPane().add(BorderLayout.NORTH, q);
    }

    /**
     * Domyślne ustawianie braku wyświetlania literek na przyciskach
     * @param buttonyIn tablica przycisków
     */
    private void firstPosition(JButton[] buttonyIn)
    {
        for(int i=0;i<25;i++) {
            buttonyIn[i].setText("");
        }
    }

    /**
     * Odpowiada za malowanie przycisków dzięki którym doszło do zwycięstwa
     * Parametry do "licznika" dla pętli for
     * @param first pierwszy przycisk z tablicy przycisków
     * @param differentBetweenNumber różnica pomiędzy następnymi przyciskami
     */
    public void paintWinButton(int first, int differentBetweenNumber) {
        int iMax = 0;
        if (differentBetweenNumber == 5)
            iMax = 25;
        else if (differentBetweenNumber == 1)
            iMax = 5;
        else if (differentBetweenNumber == 6)
            iMax = 25;
        else if (differentBetweenNumber == 4)
            iMax = 23;
        for (int i = first; i < iMax; i += differentBetweenNumber)
            buttony[i].setForeground(Color.ORANGE);
    }

    /**
     * Dodawanie słuchacz do przycisków
     * @param buttony tablica przycisków
     * @param planszaModelIn model z MVC dla planszy
     */
    public void addPushButtonListener(JButton[] buttony, PlanszaModel planszaModelIn)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                PushListener=new PushListener(buttony,planszaModelIn);
            }
        });
    }

    public Plansza getPlansza(){
        return this;
    }

    public int getButton() {
        for(int i=0;i<25;i++){
            //System.out.println("*************");
            if(buttony[i].getModel().isPressed()) {
                return i;
            }
        }

        //System.out.println("---------------");
        return 80;
    }

    public JButton[] getButtony() {
        return buttony;
    }

    public void firstPaint(String who)
    {
        if(who.equals("Server"))
            this.paintNameRed();
        if(who.equals("Client"))
            this.paintNameGreen();
    }

    /**
     * Odpowiednie malowanie kogo kolej, zielony możesz grać
     */
    public void paintNameGreen()
    {
        textName.setText("");
        textName.setForeground(Color.GREEN);
        textName.setText(theName+" Twoj ruch");
    }

    /**
     * Odpowiednie malowanie kogo kolej, na czerwono nie możesz grać
     */
    public void paintNameRed()
    {
        textName.setText("");
        textName.setForeground(Color.RED);
        textName.setText(theName+" Czekaj na ruch przeciwnika");
    }

    /**
     * Rysowanie odpowiedniego znaku na tablicy u przeciwnika na przycisku po kliknięciu
     * @param where który przycisk został naciśnięty
     * @param userChar znak który ma być narysowany na przycisku
     * Fragment oddzielony dwoma "ENTERAMI" odpowiada za usuwanie sluchaczy konkretnego przycisku w konkretnej planszy przeciwnika
     */
    public void setValue(int where,String userChar)
    {
        if(where<25)
            buttony[where].setText(userChar);
        buttony[where].setFont(new Font("Arial", Font.PLAIN, 60));


        int counter=0;
        for (JButton currentButton: buttony){
            for(ActionListener al:currentButton.getActionListeners()){
                if(counter==where)
                    currentButton.removeActionListener(al);
            }
            counter++;
        }
    }

    /**
     * Ukrywanie widoku okna
     */
    public void closeWindow()
    {
        this.setVisible(false);
    }
}
