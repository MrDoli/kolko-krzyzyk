package Client;

/**
 * Odpowiada za uruchamianie strony Clienta
 */
public class Client {
    public static void main(String[] args){
        Client client=new Client();
    }

    private ClientService clientService;

    /**
     * Konstruktor Clienta wykorzystuje metody z klasy @Client.ClientService, w których jest zaimplementowana techniczna strona działania servera
     */
    public Client(){
        clientService=new ClientService(true);
        clientService.go();
        clientService.welcomeWindow();
        while (true) {
            clientService.windowOn("Client");
            clientService.exchangeData(clientService.getPlanszaController());
        }
    }
}
