package Client;

import Controller.PlanszaController;
import Controller.WelcomWindowController;
import Model.PlanszaModel;
import Model.WelcomeWindowModel;
import Model.Win;
import View.PlanszaView;
import View.WelcomeWindowView;

import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Część techniczna Clienta (działanie)
 */
public class ClientService {
    private Socket socket;
    private boolean turnClient = true;
    private PlanszaController planszaController;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private WelcomeWindowModel welcomeWindowModel;

    /**
     * Konstruktor Client.ClientService
     * @param turnClientIn przekazuje informacje o tym czy pierwszy jest ruch Clienta (true) czy nie(false)
     */
    public ClientService(boolean turnClientIn){
        turnClient=turnClientIn;
    }

    /**
     * Odpowiada za pierwszą fazę połączenia, po prostu nawiązanie kontaku i łączności z clientem
     */
    public void go()
    {
        socket = null;
        try {/**
         Nawiazywanie polaczenia z serverem i rozwiązywanie problemu braku graczy (czeka na drugiego gracza)
         */
            while (true) {
                try {
                    socket = new Socket("localhost", 2000);
                    break;
                } catch (Exception e) {        //czy moze IOException
                    int komunikat = JOptionPane.showConfirmDialog(null,
                            "Czy chcesz czekac na serwer i drugiego gracza?", "WARNING!", JOptionPane.YES_NO_OPTION);
                    System.out.println(komunikat);

                    if (komunikat == JOptionPane.YES_OPTION) {
                        continue;
                    } else {
                        System.out.println("Wyłączono clienta");
                        socket.close();
                        System.exit(1);
                    }
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Wymiana danych pomiędzy serverem, a clientem po nawiązaniu połączenia
     * @param planszaController przekazujemy Controller Planszy
     */
    public void exchangeData(PlanszaController planszaController)
    {
        try{
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
            this.setTurnClient(true);
            while(true)
            {
                try{
                    if(this.isTurnClient()==true)
                    {
                        System.out.println(planszaController.getWhereClickedController());
                        if(planszaController.getWhereClickedController()<25)
                        {
                            //System.out.println("1-----------");
                            planszaController.paintNameRed();
                            this.setTurnClient(false);
                            outputStream.writeInt(planszaController.getWhereClickedController());
                            planszaController.setWhereClickedController(25);
                        }
                    }
                    if(planszaController.win()==true){
                        planszaController.paintWinner();
                        Win.winEnd(planszaController);
                        //System.out.println("2 WIN");
                        break;
                    }

                    if(turnClient==false)
                    {
                        //System.out.println("2------------");
                        int place=inputStream.readInt();
                        planszaController.setValuePlansza(place,planszaController.getOpponentName());
                        planszaController.setValueTab(planszaController.getOpponentName(),place);
                        planszaController.paintNameGreen();

                        this.setTurnClient(true);
                    }
                    if(planszaController.win()==true){
                        planszaController.paintWinner();
                        Win.winEnd(planszaController);
                        //System.out.println("1 WIN");
                        break;
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                    e.getMessage();
                    JOptionPane.showMessageDialog(null, "Przeciwnik opućcił grę. Koniec rozgrywki.", "UWAGA !!!", JOptionPane.WARNING_MESSAGE);
                    System.exit(0);
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Zajmuje się obsługą powitalnego okna, napisane w jednej funkcji, aby wszystko było w jednym miejscu
     */
    public void welcomeWindow()
    {
        WelcomeWindowView welcomeWindowView=new WelcomeWindowView();
        welcomeWindowModel=new WelcomeWindowModel();
        WelcomWindowController welcomWindowController=new WelcomWindowController(welcomeWindowView,welcomeWindowModel);
        while(true){
            System.out.println();
            if(welcomeWindowModel.isCanClose()==true){
                welcomeWindowView.hideWindow();
                break;
            }
        }
    }

    /**
     * Zajmuje się obsługą planszy, napisane w jednej funkcji, aby wszystko było w jednym miejscu
     * @param who server czy client
     */
    public void windowOn(String who)
    {
        PlanszaView planszaView = new PlanszaView(welcomeWindowModel.getName(), who);
        PlanszaModel planszaModel = new PlanszaModel(welcomeWindowModel.getCharElected());

        planszaController = new PlanszaController(planszaView, planszaModel);
    }

    public boolean isTurnClient() {
        return turnClient;
    }

    public void setTurnClient(boolean turnServer) {
        this.turnClient = turnServer;
    }

    public PlanszaController getPlanszaController() {
        return planszaController;
    }

    public Socket getSocket() {
        return socket;
    }

    public DataInputStream getInputStream() {
        return inputStream;
    }

    public DataOutputStream getOutputStream() {
        return outputStream;
    }
}
