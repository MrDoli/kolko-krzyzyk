package Server;

import Controller.PlanszaController;
import Controller.WelcomWindowController;
import Model.PlanszaModel;
import Model.WelcomeWindowModel;
import Model.Win;
import View.PlanszaView;
import View.WelcomeWindowView;

import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Część techniczna Servera (działanie)
 */
public class ServerService {
    Socket client=null;
    ServerSocket server=null;
    private boolean turnServer = false;
    private PlanszaController planszaController;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private WelcomeWindowModel welcomeWindowModel;

    /**
     * Konstruktor Server.ServerService
     * @param turnServerIn przekazuje informacje o tym czy pierwszy jest ruch Servera (true) czy nie(false)
     */
    public ServerService(boolean turnServerIn){
        setTurnServer(turnServerIn);
    }

    /**
     * Odpowiada za pierwszą fazę połączenia, po prostu nawiązanie kontaku i łączności z clientem
     */
    public void go()
    {
        client=null;
        server=null;

        try{
            server = new ServerSocket(2000);//utowrzenie gniazda
            System.out.println("Czekamy na połączenie...");
            JOptionPane.showMessageDialog(null, "Czekam na połączenie z clientem, \n czyli drugim graczem :D", "Informacja", JOptionPane.WARNING_MESSAGE);
            client = server.accept();

            System.out.println("Client.Client został podłączony " + client.getInetAddress().getHostName());// to można zrobić za pomocą swinga ładnie w oknie
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Zajmuje się obsługą powitalnego okna, napisane w jednej funkcji, aby wszystko było w jednym miejscu
     */
    public void welcomeWindow()
    {
        WelcomeWindowView welcomeWindowView=new WelcomeWindowView();
        welcomeWindowModel=new WelcomeWindowModel();
        WelcomWindowController welcomWindowController=new WelcomWindowController(welcomeWindowView,welcomeWindowModel);
        while(true){
            System.out.println();
            if(welcomeWindowModel.isCanClose()==true){
                welcomeWindowView.hideWindow();
                break;
            }
        }
    }

    /**
     * Zajmuje się obsługą planszy, napisane w jednej funkcji, aby wszystko było w jednym miejscu
     * @param who server czy client
     */
    public void windowOn(String who)
    {
        PlanszaView planszaView = new PlanszaView(welcomeWindowModel.getName(), who);
        PlanszaModel planszaModel = new PlanszaModel(welcomeWindowModel.getCharElected());

        planszaController = new PlanszaController(planszaView, planszaModel);
    }

    /**
     * Wymiana danych pomiędzy serverem, a clientem po nawiązaniu połączenia
     * @param planszaController przekazujemy Controller Planszy
     */
    public void exchangeData(PlanszaController planszaController)
    {
        try{
            DataInputStream inputStream = new DataInputStream(client.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(client.getOutputStream());
            this.setTurnServer(false);
            while(true)
            {
                try{
                    if(this.isTurnServer()==true)
                    {
                        System.out.println(planszaController.getWhereClickedController());
                        if(planszaController.getWhereClickedController()<25)
                        {
                            //System.out.println("1-----------");
                            planszaController.paintNameRed();
                            this.setTurnServer(false);
                            outputStream.writeInt(planszaController.getWhereClickedController());
                            planszaController.setWhereClickedController(25);
                        }
                    }
                    if(planszaController.win()==true){
                        planszaController.paintWinner();
                        Win.winEnd(planszaController);
                        //System.out.println("2 WIN");
                        break;
                    }

                    if(turnServer==false)
                    {
                        //System.out.println("2------------");
                        int place=inputStream.readInt();
                        planszaController.setValuePlansza(place,planszaController.getOpponentName());
                        planszaController.setValueTab(planszaController.getOpponentName(),place);
                        planszaController.paintNameGreen();

                        this.setTurnServer(true);
                    }
                    if(planszaController.win()==true){
                        planszaController.paintWinner();
                        Win.winEnd(planszaController);
                        //System.out.println("1 WIN");
                        break;
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                    e.getMessage();
                    JOptionPane.showMessageDialog(null, "Przeciwnik opućcił grę. Koniec rozgrywki.", "UWAGA !!!", JOptionPane.WARNING_MESSAGE);
                    System.exit(0);
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public boolean isTurnServer() {
        return turnServer;
    }

    public void setTurnServer(boolean turnServer) {
        this.turnServer = turnServer;
    }

    public PlanszaController getPlanszaController() {
        return planszaController;
    }

    public Socket getClient() {
        return client;
    }

    public DataInputStream getInputStream() {
        return inputStream;
    }

    public DataOutputStream getOutputStream() {
        return outputStream;
    }
}
