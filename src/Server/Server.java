package Server; /**
 * @author Marcin Dolicher
 * @version 1.0
 * Poniższy program to popularna gra sieciowa kółko-krzyżyk.
 */


/**
 * Odpowiada za uruchamianie strony Servera
 */
public class Server {
    public static void main(String[] args) {
        Server server=new Server();
    }

    private ServerService serverService;

    /**
     * Konstruktor servera wykorzystuje metody z klasy @Server.ServerService, w których jest zaimplementowana techniczna strona działania servera
     */
    Server(){
        serverService=new ServerService(false);
        serverService.go();
        serverService.welcomeWindow();
        while (true) {
            serverService.windowOn("Server");
            serverService.exchangeData(serverService.getPlanszaController());
        }
    }

}
