package Controller;

import Model.PlanszaModel;
import View.Plansza;
import View.PlanszaView;

import javax.swing.*;

/**
 * Controller z MVC dla WelcomeWindow
 */
public class PlanszaController {
    private PlanszaView thePlanszaView;
    private PlanszaModel thePlanszaModel;
    private Plansza thePlansza;

    /**
     * Konstruktor glównego Controllera w programie
     * @param planszaView główny element View
     * @param planszaModel główny element Model.Model-u
     */
    public PlanszaController(PlanszaView planszaView, PlanszaModel planszaModel)
    {
        this.thePlanszaView=planszaView;
        this.thePlanszaModel=planszaModel;
        this.thePlansza=planszaView.getPlanszaFromView();

        this.thePlansza.addPushButtonListener(thePlansza.getButtony(), planszaModel);
    }

    /**
     * Pobiera informacje który JButton został kliknięty
     * @return zwraca int czyli informację, który konkretnie przycisk kliknięto
     */
    public int getWhereClickedController() {
        return thePlanszaModel.getWhereClicked();
    }

    /**
     * Zapisuje informację, który JButton został kliknięty
     * @param clicked liczba odpowiadająca konkretnemu przyciskowi
     */
    public void setWhereClickedController(int clicked){
        thePlanszaModel.setWhereClicked(clicked);
    }

    /**
     * Malowanie na zielono imienia - można grać
     */
    public void paintNameGreen(){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                thePlansza.paintNameGreen();
            }
        });
    }

    /**
     * Malowanie na czerwono imienia - nie można grać
     */
    public void paintNameRed() {
        SwingUtilities.invokeLater(new Runnable() {
        @Override
         public void run() {
            thePlansza.paintNameRed();
          }
        });
    }

    /**
     * Pobieranie imienia przeciwnika
     * @return imie przeciwnika
     */
    public String getOpponentName()
    {
      return thePlanszaModel.getOpponent();
    }

    /**
     * Uruchamianie funkcji setValue z Planszy
     * @see Plansza
     * @param where który przycisk został naciśnięty
     * @param userChar znak który ma być narysowany na przycisku
     */
    public void setValuePlansza(int where,String userChar){
        thePlansza.setValue(where, userChar);
    }

    /**
     * Uruchamianie funkcji setValueInTab z Modelu Planszy
     * @see PlanszaModel
     * @param user typ X/O
     * @param where miejsce w tablicy
     */
    public void setValueTab(String user, int where){
        thePlanszaModel.setValueInTab(user,where);
    }

    /**
     * Sprawdzanie czy wygrana
     * @return zwraca wartość boolean true-wygrana, false-brak wygranej
     */
    public boolean win(){
        return thePlanszaModel.whoWin();
    }

    /**
     * Zaznaczanie zwycięskich przycików
     * @see Plansza
     */
    public void paintWinner()
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                thePlansza.paintWinButton(thePlanszaModel.getFirstWin(), thePlanszaModel.getDifferentBetweenNumberWin());
            }
        });
    }

    public boolean getiWinController(){
        return thePlanszaModel.getiWin();
    }

    public void closeWindow(){
        thePlansza.closeWindow();
    }
}
