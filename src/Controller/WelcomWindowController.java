package Controller;

import Model.WelcomeWindowModel;
import View.WelcomWindow;
import View.WelcomeWindowView;

/**
 * Controller z MVC dla WelcomeWindow
 */
public class WelcomWindowController {
    private WelcomeWindowView theWelcomeView;
    private WelcomeWindowModel theWelcomeWindowModel;
    private WelcomWindow theWelcomWindow;

    /**
     * Konstruktor tworzący Controller dla Welcome Window
     * @param welcomeWindowView View dla Welcome Window
     * @param welcomeWindowModel Model.Model dla Welcome Window
     */
    public WelcomWindowController(WelcomeWindowView welcomeWindowView, WelcomeWindowModel welcomeWindowModel)
    {
        this.theWelcomeView=welcomeWindowView;
        this.theWelcomeWindowModel=welcomeWindowModel;
        this.theWelcomWindow=welcomeWindowView.getWelcomWindow();

        this.theWelcomWindow.addElectedButtonListener(theWelcomWindow.getO(),theWelcomeWindowModel);
        this.theWelcomWindow.addElectedButtonListener(theWelcomWindow.getx(),theWelcomeWindowModel);
        this.theWelcomWindow.addOkButtonListener(theWelcomWindow.getOk(),theWelcomWindow.getForNick(),theWelcomeWindowModel);
    }
}