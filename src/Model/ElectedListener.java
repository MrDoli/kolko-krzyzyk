package Model;

import Model.WelcomeWindowModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Obsługa zdarzeń dla przycisków z Welcome Window
 */
public class ElectedListener {
    /**
     * Konstruktor dzięki któremu dodajemu obsługe zdarzeń dla przycisków z Welcome Window
     * @param buttonIn przycisk X/O
     * @param welcomeWindowModelIn Model.Model dla Welcome Window
     */
    public ElectedListener(JButton buttonIn, WelcomeWindowModel welcomeWindowModelIn)
    {
        String onButton=buttonIn.getText();
        if(onButton.equals("O"))
        {
            buttonIn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    welcomeWindowModelIn.setCharElected("O");
                    buttonIn.setForeground(Color.RED);
                }
            });
        } else if(onButton.equals("X"))
        {
            buttonIn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    welcomeWindowModelIn.setCharElected("X");
                    buttonIn.setForeground(Color.RED);
                }
            });
        }
    }

    /**
     * Obsługa zdarzeń dla przycisku "OK"
     * @param buttonIn przycisk "OK"
     * @param textFieldIn obszer tekstowy w którym wpisujemy imie/nick gracza
     * @param welcomeWindowModelIn Model.Model dla Welcome Window
     */
    public ElectedListener(JButton buttonIn,JTextField textFieldIn, WelcomeWindowModel welcomeWindowModelIn)
    {
        buttonIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String onName=textFieldIn.getText();
                welcomeWindowModelIn.setName(onName);
                //System.out.println("OK");
                welcomeWindowModelIn.setCanClose(true);
            }
        });
    }

}
