package Model;

public interface ModelWelcomeWindow {
    public void setCharElected(String charElected);
    public String getCharElected();
    public void setName(String name);
    public String getName();
    public void setCanClose(boolean canClose);
    public boolean isCanClose();
}
