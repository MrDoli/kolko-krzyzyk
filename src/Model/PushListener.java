package Model;

import Model.PlanszaModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Obsługa zdarzeń dla przycisków z Planszy
 */
public class PushListener{
    private PlanszaModel thePlanszaModel;

    public PushListener() {
    }

    /**
     * Konstruktor dzięki któremu dodajemu obsługe zdarzeń dla przycisków z Planszy
     * @param buttonsIn tablica z przyciskami
     * @param planszaModel Model.Model z MVC dla Planszy
     */
    public PushListener(JButton[] buttonsIn, PlanszaModel planszaModel)
    {
        this.thePlanszaModel=planszaModel;
        for(int i=0;i<25;i++)
        {
            final int finalI=i;
            buttonsIn[finalI].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    System.out.println(finalI);
                    buttonsIn[finalI].setText(thePlanszaModel.getUser());
                    buttonsIn[finalI].setFont(new Font("Arial", Font.PLAIN, 60));
                    buttonsIn[finalI].removeActionListener(this);
                    thePlanszaModel.setValueInTab(thePlanszaModel.getUser(), finalI);
                    thePlanszaModel.showWartTab();
                    thePlanszaModel.setWhereClicked(finalI);
                }
            });
        }
    }
}
