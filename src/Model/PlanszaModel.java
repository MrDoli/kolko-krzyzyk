package Model;

import Model.Model;

public class PlanszaModel implements Model {
    private String user;
    /**
     * @wartTab - tablica przechowująca wartości kliknięć, na jej podstawie sprawdzmy kto wygrał,
     * (taki mały zbiór danych dla programu)
     */
    private char wartTab[]=new char[25];
    private boolean win=false;
    private int whereClicked;
    private String opponent;
    private int firstWin;
    private int differentBetweenNumberWin;
    private boolean iWin;

    /**
     * Konstruktor modelu
     * @param user imie/nick gracza
     */
    public PlanszaModel(String user){
        this.user=user;
        this.makeTab(25);
        whereClicked=26;
        this.chooseOpponenet(user);
        iWin=false;
    }

    /**
     * Wyswietlanie w konsoli wartosci z tablicy - przy tworzeniu oprogramowania słuzyło do
     * wizualnej kontroli czy wszystko przebiega dobrze
     */
    @Override
    public void showWartTab()
    {
        for (char viewTab:wartTab) {
            //System.out.print(viewTab);
        }
    }

    /**
     * Ustawianie X/O w zależności od tego czy gracz jest X/O
     * @param user typ X/O
     * @param where miejsce w tablicy
     */
    @Override
    public void setValueInTab(String user, int where)
    {
        if(user.equals("X"))    // WIELKI "X"
            wartTab[where]='x';
        else if(user.equals("O"))   // WIELKI "O"
            wartTab[where]='o';
        else
            throw new RuntimeException("BAD CHAR");
    }

    public String getUser() {
        return user;
    }

    /**
     * ustawia domyslny stan tablicy, wszystkie pola zawierają "g"
     * @param ilosc jak duża ma być tablica
     */
    @Override
    public void makeTab(int ilosc)
    {
        for (int i = 0; i < ilosc; i++)
            wartTab[i]='g';
    }

    /**
     * Sprawdza w tablicy warunki na wygraną
     * @return zwraca wartość boolean true-wygrana, false-brak wygranej
     */
    @Override
    public boolean whoWin()
    {
        char znak='o';
        boolean tempWin;
        tempWin=winService(znak);
        if(tempWin==true)
            return tempWin;
        znak+=9;
        return tempWin=winService(znak);
    }

    /**
     * Porusza się po tablicy i sprawdza czy zaszły warunki na wygraną - działanie algorytmiczne
     * @param znak dla kogo sprawdzamy x/o
     * @return true-wygrana, false-przegrana
     */
    @Override
    public boolean winService(char znak)
    {
        for(int i=0;i<25;i+=5)    //w poziomie
        {
            if(wartTab[i]==znak && wartTab[i+1]==znak && wartTab[i+2]==znak && wartTab[i+3]==znak && wartTab[i+4]==znak) {
                setWin(true);
                setFirstWin(i);
                setDifferentBetweenNumberWin(1);
                proofWhoWin(znak);
                return this.win;
            }
        }
        for(int i=0;i<5;i++)    //w pionie
        {
            if(wartTab[i]==znak && wartTab[i+5]==znak && wartTab[i+10]==znak && wartTab[i+15]==znak && wartTab[i+20]==znak) {
                setWin(true);
                setFirstWin(i);
                setDifferentBetweenNumberWin(5);
                proofWhoWin(znak);
                return this.win;
            }
        }
        int i=0;    // po ukasie raz
        if(wartTab[0]==znak && wartTab[6]==znak && wartTab[12]==znak && wartTab[18]==znak && wartTab[24]==znak) {
            setWin(true);
            setFirstWin(0);
            setDifferentBetweenNumberWin(6);
            proofWhoWin(znak);
            return this.win;
        }
        //po ukosie dwa
        if(wartTab[4]==znak && wartTab[8]==znak && wartTab[12]==znak && wartTab[16]==znak && wartTab[20]==znak) {
            setWin(true);
            setFirstWin(4);
            setDifferentBetweenNumberWin(4);
            proofWhoWin(znak);
            return this.win;
        }
        return false;
    }

    /**
     * Ustawia zmienną @iWin na true, gdy konkretny gracz wygra wykorzystywane w funkcji @winSerice
     * @param znak dla kogo sprawdzamy x/o
     */
    @Override
    public void proofWhoWin(char znak)
    {
        if(getUser().equals("X"))
        {
            if(znak=='x')
                this.setiWin(true);
        } else if(getUser().equals("O"))
        {
            if(znak=='o')
                this.setiWin(true);
        }
    }

    public void setiWin(boolean iWin) {
        this.iWin = iWin;
    }

    public boolean getiWin(){
        return this.iWin;
    }

    @Override
    public void setWhereClicked(int buttonClicked) {
        this.whereClicked=buttonClicked;
    }

    @Override
    public int getWhereClicked() {
        return this.whereClicked;
    }

    /**
     * Wybieramy czy przeciwnik jest X/O
     * @param user dostarcza informacji o użytkowniku czy jest X/O
     */
    public void chooseOpponenet(String user)
    {
        if(user.equals("X"))
            opponent="O";
        else if(user.equals("O"))
            opponent="X";
    }

    public String getOpponent() {
        return opponent;
    }

    public void setFirstWin(int firstWin) {
        this.firstWin = firstWin;
    }

    public int getFirstWin() {
        return firstWin;
    }

    public void setDifferentBetweenNumberWin(int differentBetweenNumberWin) {
        this.differentBetweenNumberWin = differentBetweenNumberWin;
    }

    public int getDifferentBetweenNumberWin() {
        return differentBetweenNumberWin;
    }

    public void setWin(boolean win) {
        this.win = win;
    }
}
