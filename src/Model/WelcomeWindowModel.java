package Model;

import Model.ModelWelcomeWindow;

/**
 * Model.Model z MVC dla WelcomeWindow
 */
public class WelcomeWindowModel implements ModelWelcomeWindow {
    /**
     * @charEleted to znak, który wybrał gracz X/O
     * @name imię, które podał gracz
     * @canClose zmienna informujący czy wszystkie dane zostały pobrane od użytkownika
     */
    String charElected;
    String name;
    boolean canClose=false;

    /**
     * Ustawia znak gracza
     * @param charElected to znak, który wybrał gracz X/O
     */
    @Override
    public void setCharElected(String charElected) {
        this.charElected = charElected;
    }

    /**
     * Zwraca wybrany znak gracza
     * @return znak, który wybrał gracz X/O
     */
    @Override
    public String getCharElected() {
        return charElected;
    }

    /**
     * Ustawia imie gracza/nick
     * @param name imię, które podał gracz
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Zwraca imię/nick podane przez gracza
     * @return imie/nick gracza
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Ustawia wartość boolean true-gdy można zamknąć okno (wszystkie dane pobrane), false-gdy nie
     * @param canClose zmienna informujący czy wszystkie dane zostały pobrane od użytkownika
     */
    @Override
    public void setCanClose(boolean canClose) {
        this.canClose = canClose;
    }

    /**
     * Zwraca informacje czy wszystkie dane zostały pobrane
     * @return zmienna informujący czy wszystkie dane zostały pobrane od użytkownika
     */
    @Override
    public boolean isCanClose() {
        return canClose;
    }
}
