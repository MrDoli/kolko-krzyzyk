package Model;

public interface Model {
    /**
     * Wyswietlanie w konsoli wartosci z tablicy - przy tworzeniu oprogramowania słuzyło do
     * wizualnej kontroli czy wszystko przebiega dobrze
     */
    public void showWartTab();

    /**
     * Ustawianie X/O w zależności od tego czy gracz jest X/O
     * @param user typ X/O
     * @param where miejsce w tablicy
     */
    public void setValueInTab(String user, int where);

    /**
     * ustawia domyslny stan tablicy, wszystkie pola zawierają "g"
     * @param ilosc jak duża ma być tablica
     */
    public void makeTab(int ilosc);

    /**
     * Sprawdza w tablicy warunki na wygraną
     * @return zwraca wartość boolean true-wygrana, false-brak wygranej
     */
    public boolean whoWin() ;


    public void setWhereClicked(int buttonClicked);
    public int getWhereClicked();

    /**
     * Porusza się po tablicy i sprawdza czy zaszły warunki na wygraną - działanie algorytmiczne
     * @param znak dla kogo sprawdzamy x/o
     * @return true-wygrana, false-przegrana
     */
    public boolean winService(char znak);

    /**
     * Ustawia zmienną @iWin na true, gdy konkretny gracz wygra wykorzystywane w funkcji @winSerice
     * @param znak dla kogo sprawdzamy x/o
     */
    public void proofWhoWin(char znak);
}
