package Model;

import Controller.PlanszaController;

import javax.swing.*;

/**
 * Klasa obsługująca działanie programu po wykryciu zwycięstwa
 */
public class Win {
    /**
     * Funkcja odpowiadająca za działanie programu po wykryciu zwycięstwa (wygranej jedenego z graczy)
     * @param planszaController Controller dla Planszy
     */
    static public void winEnd(PlanszaController planszaController)
    {
        int input=6;
        long time=System.currentTimeMillis();
        while(true)
        {
            if (planszaController.getiWinController() == true)
            {
                input = JOptionPane.showOptionDialog(null, "Wygrałeś\nJezeli chcesz zaczac nowa rozgrywke nacisnij OK", "Koniec rozgrywki", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            } else if (planszaController.getiWinController() == false)
            {
                input = JOptionPane.showOptionDialog(null, "Przegrałeś\nJezeli chcesz zaczac nowa rozgrywke nacisnij OK", "Koniec rozgrywki", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            }
            if (input == 0) {
                planszaController.closeWindow();
                return;
            }
            if(input==2){
                System.exit(1);
            }
            long tempTime=System.currentTimeMillis();
            tempTime=tempTime-time;
            if(tempTime>10000) {
                JOptionPane.showMessageDialog(null, "Przekroczono czas oczekiwania\nPo nacisnięciu OK program zostanie wyłączony", "Koniec rozgrywki", JOptionPane.WARNING_MESSAGE);//, null, null, null);
                System.exit(0);
            }
        }

    }
}
